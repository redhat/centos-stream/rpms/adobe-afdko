# Adobe Font Development Kit for OpenType ([AFDKO](https://adobe-type-tools.github.io/afdko/))

The AFDKO is a set of tools for building OpenType font files from PostScript and TrueType font data.

This repository contains the data files, Python scripts, and sources for the command line programs that comprise the AFDKO. The project uses the [Apache 2.0 OpenSource license](https://github.com/adobe-type-tools/afdko/blob/develop/LICENSE.md).

Please refer to the [AFDKO Overview](https://adobe-type-tools.github.io/afdko/AFDKO-Overview.html) for a more detailed description of what is included in the package.

Please see the [wiki](https://github.com/adobe-type-tools/afdko/wiki) for additional information, such as links to reference materials and related projects.
